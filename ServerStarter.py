
import atexit
import os
import json
import subprocess
import sys

from _gui import GUIMaker
from _processes import Program, ProgramClient, ProgramServer, ProcessStatus
from _viewed_server import ViewedServerProcesses
from _viewed_client import ViewedClientProcesses

# Import tkinter
try:
  import Tkinter as tk
  from tkinter import messagebox
except ImportError:
  import tkinter as tk
  from tkinter import messagebox

# == FlyFF Server Starter by SquonK, 2019-2023 ==
# The intent of this program is to fluidify the workflow by having an easy way to update servers executable
# for local testing purposes.


# Global launcher
class ServerStarter:
  servers: ViewedServerProcesses
  client: ViewedClientProcesses
  
  # Constructor
  def __init__(self, config, tk_root, interface):
    self.client = ViewedClientProcesses.setup_from_json(config['client'], interface)
    self.servers = ViewedServerProcesses.setup_from_json(config['server'], tk_root, interface)
  
  
  def open_dir(self):
    os.startfile("..\\")
  
  def on_tick(self):
    self.servers.update_status()        
    self.client.update_status()

  def on_exit(self):
    self.servers.kill_all()
    self.client.process.kill_all_process()


# ==== LINK WITH UI ====

if __name__ == '__main__':
  root = tk.Tk()
  interface = GUIMaker(root)
  
  try:
    with open('config.json', 'r') as file:
      config = json.load(file)
  except Exception as e:
    tk.messagebox.showerror("Error", f"An error has occurred: {e}")
    raise e

  flyff = ServerStarter(config, root, interface)
  
  # On close
  atexit.register(flyff.on_exit)
  
  # Open dir button
  interface.button_open_flyff_dir.configure(command=flyff.open_dir)
  
  # Git Button
  if "git" in config:
    def git_opener():
      subprocess.Popen(config["git"]["executable"], cwd=config["git"]["repository_path"])
    interface.button_git.configure(command=git_opener)
  else:
    interface.button_git.place_forget()
  
  # Reset status every second
  def tick():
    flyff.on_tick()
    root.after(1000, tick)
      
  tick()

  root.mainloop()

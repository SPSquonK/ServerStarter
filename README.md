# Server Starter

This is a python program intended to FlyFF developers to make it easier to
start / restart the parts of the server  during development.

![A screenshot](Screenshot.png)

Server Starters enables the user to:
- Start the server by clicking on one button
- If an executable is recompiled, Server Starter will see that the executable
changed, and on the next start, will copy the executable from the compile
folder to the execution folder before starting it.

**Advantages**:
- You don't have to copy your files from folder to folder anymore
- You can compile while still running your server

**Drawbacks**:
- None!

> *"I have been using ServerStarter for 4 years, and it never failed me... wait I am
the author of this tool, why am I being interviewed? Can't you find real
users?"* SquonK, 2023



## Install the script

This script uses python, so be sure python is installed on your system. As
FlyFF is entirely running on Windows, the script is made to be compatible with
Windows only.

- The program was developed on Python 3.10.
    - Compatibility with other Python version has not been tested.
    - For Python 3.6, you have to install Tkinter by yourself.


### Installation

*TL;DR: Just clone this in your FlyFF folder and it should work.*

- This repository should be cloned in a repertory next to the Source, Program and Resource folders.
    - For example, if your FlyFF development folder is `B:\FlyFF\`, your output folder should be `B:\FlyFF\Output`, your resource folder should be `B:\FlyFF\Resource`, and the files of this repository will be for example in `B:\FlyFF\Starter`.
    - More precisely, the Output folder has to be relatively to the script `..\Output`, the Program folder has to be `..\Program` and the Resource folder has to be `..\Resource`
    - If your folders are named differently, search for occurrences of these folders to replace them with the one corresponding to your project in `Launcher.py`
    - The path of the executables can be changed in the `config.json` file.


### Running
- The program can be started by running the provided bat file `./start.bat` or
`python ServerStarter.py`


## Features

- Start the different programs that compose the server and Neuz instances

- The script tracks every second if new compiled files are available, and displays it. When pressing Start, if the server is running and some parts are either off or obsoleted by a new compilation, the script will shut down the needed parts, update the executables, and restart the server.

- Windows can be hidden to lighten the taskbar by checking the checkbox before starting the server.


## Code organization

- `_processes.py` manages the processes
- `_gui.py` produces the Tkinter interface
- `_viewed_client.py` and `_viewed_server.py` connects the processes with the GUI.
- `ServerStarter.py` is sitting there, watching other files work.


## Misc.

- This script is only intended for development purpose, and should not be used in production. The main reason you should not use this in production context is that the main perk of Server Starter, tracking for newly compiled executables and replacing started processes, is totally useless in production.


## License

Distributed under the MIT License by SquonK.

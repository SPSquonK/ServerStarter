try:
  import Tkinter as tk
except ImportError:
  import tkinter as tk

class GUIMaker:
  @staticmethod
  def configure_button(button: tk.Button):
    button.configure(activebackground="#ececec")
    button.configure(activeforeground="#000000")
    button.configure(background="#d9d9d9")
    button.configure(disabledforeground="#a3a3a3")
    button.configure(foreground="#000000")
    button.configure(highlightbackground="#d9d9d9")
    button.configure(highlightcolor="black")
    button.configure(pady="0")

  @staticmethod
  def configure_message(message: tk.Message):
    message.configure(background="#d9d9d9")
    message.configure(foreground="#000000")
    message.configure(highlightbackground="#d9d9d9")
    message.configure(highlightcolor="black")


  button_open_flyff_dir: tk.Button
  button_git: tk.Button

  def __init__(self, top):
    self.top = top

    top.geometry("511x307+1247+587")
    top.title("FlyFF Server Starter")
    top.configure(relief="ridge")
    top.configure(background="#d9d9d9")
    top.configure(highlightbackground="#d9d9d9")
    top.configure(highlightcolor="black")

    self._display_spam()

  def _display_spam(self):
    top = self.top

    menubar = tk.Menu(top, font="TkMenuFont",bg='#d9d9d9',fg='#000000')
    top.configure(menu = menubar)

    credit_message = tk.Message(top)
    credit_message.place(relx=0.724, rely=0.847, relheight=0.107, relwidth=0.254)
    GUIMaker.configure_message(credit_message)
    credit_message.configure(justify='center')
    credit_message.configure(text='''FlyFF Server Launcher\nSquonK, 2019''')
    credit_message.configure(width=130)

    self.button_open_flyff_dir = tk.Button(top)
    self.button_open_flyff_dir.place(relx=0.509, rely=0.847, height=34, width=97)
    self.button_open_flyff_dir.configure(text='''Open FlyFF Dir''')
    GUIMaker.configure_button(self.button_open_flyff_dir)

    self.button_git = tk.Button(top)
    self.button_git.place(relx=0.939, rely=0.912, height=24, width=27)
    self.button_git.configure(text='''Git''')
    GUIMaker.configure_button(self.button_git)


  def build_server_frame(self, number_of_executables: int):
    top = self.top

    # The frame
    server_frame = tk.LabelFrame(top)
    server_frame.place(relx=0.02, rely=0.033, relheight=0.928, relwidth=0.47)
    server_frame.configure(relief='groove')
    server_frame.configure(foreground="black")
    server_frame.configure(text='''Server''')
    server_frame.configure(background="#d9d9d9")
    server_frame.configure(highlightbackground="#d9d9d9")
    server_frame.configure(highlightcolor="black")

    # Buttons common to all servers
    button_start = tk.Button(server_frame)
    button_start.place(relx=0.042, rely=0.807, relheight=0.154, relwidth=0.416, bordermode='ignore')
    button_start.configure(text='''Start''')
    GUIMaker.configure_button(button_start)

    buttton_stop = tk.Button(server_frame)
    buttton_stop.place(relx=0.542, rely=0.807, relheight=0.154, relwidth=0.416, bordermode='ignore')
    buttton_stop.configure(text='''Stop''')
    GUIMaker.configure_button(buttton_stop)

    # Buttons for each server
    server_controls = []

    def make_server_controls(rely):
      name_control = tk.Message(server_frame)
      name_control.place(relx=0.208, rely=rely, relheight=0.081, relwidth=0.5 , bordermode='ignore')
      name_control.configure(width=120)
      GUIMaker.configure_message(name_control)

      state_control = tk.Message(server_frame)
      state_control.place(relx=0.75, rely=rely, relheight=0.081, relwidth=0.208, bordermode='ignore')
      state_control.configure(width=50)
      GUIMaker.configure_message(state_control)

      hide_control = tk.Checkbutton(server_frame)
      hide_control.place(relx=0.042, rely=rely, relheight=0.088, relwidth=0.088, bordermode='ignore')
      hide_control.configure(activebackground="#d9d9d9")
      hide_control.configure(activeforeground="#000000")
      hide_control.configure(background="#d9d9d9")
      hide_control.configure(disabledforeground="#a3a3a3")
      hide_control.configure(foreground="#000000")
      hide_control.configure(highlightbackground="#d9d9d9")
      hide_control.configure(highlightcolor="black")
      hide_control.configure(justify='left')

      return { 'title': name_control, 'state': state_control, 'check': hide_control }

    srvr_controls_top = 0.07
    srvr_controls_bottom = 0.702 + 0.081
    srvr_controls_offset = (srvr_controls_bottom - srvr_controls_top) / number_of_executables

    server_controls = [
      make_server_controls(srvr_controls_top + srvr_controls_offset * i)
      for i in range(number_of_executables)
    ]

    return server_controls, button_start, buttton_stop


  def build_client_frame(self):
    top = self.top

    self.client_frame = tk.LabelFrame(top)
    self.client_frame.place(relx=0.509, rely=0.033, relheight=0.798 , relwidth=0.47)
    self.client_frame.configure(relief='groove')
    self.client_frame.configure(foreground="black")
    self.client_frame.configure(text='''Client''')
    self.client_frame.configure(background="#d9d9d9")
    self.client_frame.configure(highlightbackground="#d9d9d9")
    self.client_frame.configure(highlightcolor="black")

    client_message_1 = tk.Message(self.client_frame)
    client_message_1.place(relx=0.042, rely=0.082, relheight=0.094, relwidth=0.917, bordermode='ignore')
    client_message_1.configure(width=220)
    GUIMaker.configure_message(client_message_1)

    client_message_2 = tk.Message(self.client_frame)
    client_message_2.place(relx=0.042, rely=0.204, relheight=0.094, relwidth=0.917, bordermode='ignore')
    client_message_2.configure(width=220)
    GUIMaker.configure_message(client_message_2)

    return client_message_1, client_message_2


  def add_client_buttons(self, actions: list):
    relys = [0.816, 0.653, 0.49, 0.327]
    relheight = 0.138

    if len(actions) > 4:
      relheight = (relys[0] + relheight - relys[-1]) / len(actions)
      relys = [0.816 + 0.138 - (i + 1) * relheight for i in range (len(actions))]

    buttons = []

    for (action, rely) in zip(actions, relys):
      button = tk.Button(self.client_frame)
      button.place(relx=0.042, rely=rely, relheight=relheight, relwidth=0.9, bordermode='ignore')
      GUIMaker.configure_button(button)
      button.configure(text=action['text'])
      button.configure(command=action['command'])

      buttons.append(button)

    return buttons

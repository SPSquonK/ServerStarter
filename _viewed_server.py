from _processes import ProgramServer, ProcessStatus
from threading import Thread

try:
  import Tkinter as tk
except ImportError:
  import tkinter as tk

# Adaptors of the classes in _processes with Tkinter


def boolean_to_tk_active(boolean: bool) -> str:
  if boolean:
    return "active"
  else:
    return "disabled"

status_text = {
  ProcessStatus.STATUS_STARTED : "ON",
  ProcessStatus.STATUS_OFF     : "OFF",
  ProcessStatus.STATUS_OBSOLETE: "UPDATE"
}

status_color = {
  ProcessStatus.STATUS_STARTED : "#005000",
  ProcessStatus.STATUS_OFF     : "#FF0000",
  ProcessStatus.STATUS_OBSOLETE: "#FF8000"
}

class ViewedServerProcess:
  process: ProgramServer
  normalized_name: str

  def __init__(self, process, controls):
    self.process = process
    self.controls = controls
            
    self.normalized_name = self.process.name
    if not self.normalized_name.endswith("Server"):
      self.normalized_name = self.normalized_name + "Server"

    self._setup_controls()

  def _setup_controls(self):
    self.controls['title'].configure(text=self.process.name)
    
    if self.process.hide:
      state = tk.NORMAL
      self.controls['check'].deselect()
    else:
      state = tk.ACTIVE
      self.controls['check'].select()
    
    def change_hide():
      self.process.hide = not self.process.hide
    
    self.controls['check'].configure(state=state)
    self.controls['check'].configure(command=change_hide)


  def update_status(self):
    process_status = self.process.get_status()

    self.controls['title'].configure(foreground=status_color.get(process_status, "black"))
    self.controls['state'].configure(foreground=status_color.get(process_status, "black"))
    self.controls['state'].configure(text      =status_text .get(process_status, "Unknown"))

  def add_contextual_menu(self, tk_root):
    def create_contextual(event):
      normalized_name = self.process.name

      active_state = boolean_to_tk_active(self.process.is_running())
      not_active_state = boolean_to_tk_active(not self.process.is_running())

      menu = tk.Menu(tk_root, tearoff=0)
      menu.add_command(label="Start " + normalized_name, state=not_active_state, command=self.process.solo_start)
      menu.add_command(label="Stop " + normalized_name, state=active_state, command=self.process.kill)
      menu.add_separator()
      menu.add_command(label="Start until " + normalized_name, state=not_active_state, command=self.process.start)
      menu.add_command(label="Stop from " + normalized_name, state=active_state, command=self.process.kill_all_process)
      menu.add_separator()
      menu.add_command(label="Show/Hide", state=active_state, command=self.process.change_visibility)

      menu.post(event.x_root, event.y_root)

    for component in [self.controls['title'], self.controls['state']]:
      component.bind("<Button-3>", create_contextual)


class ViewedServerProcesses:
  servers: list[ViewedServerProcess]
  start_button: tk.Button
  stop_button: tk.Button

  @staticmethod
  def setup_from_json(json_content, tk_root, gui_maker):
    srvr_controls, start_button, stop_button = gui_maker.build_server_frame(len(json_content))

    viewed_servers = []
    raw_previous = None
    for (server, controls) in zip(json_content, srvr_controls):
      program = ProgramServer(
        server['name'],
        server['run_path'],
        server['working_directory'],
        server['process_titles'],
        server['compile_path'],
        0,
        not server['show'],
        server['arguments'] if 'arguments' in server else []
      )

      if raw_previous is not None:
        program.add_dependency(raw_previous)
        raw_previous.timeout = 0.5

      viewed = ViewedServerProcess(program, controls)
      viewed.add_contextual_menu(tk_root)

      viewed_servers.append(viewed)

      raw_previous = program
    
    return ViewedServerProcesses(viewed_servers, start_button, stop_button)


  def __init__(self, servers: list[ViewedServerProcess], start_button, stop_button):
    self.servers = servers
    self.start_button = start_button
    self.stop_button = stop_button

    self.start_button.configure(command=self.start_in_new_thread)
    self.stop_button.configure(command=self.kill_all)


  def update_status(self):
    for server in self.servers:
      server.update_status()
  
  def kill_all(self):
    for server in self.servers:
      server.process.kill()

  def start_all(self) -> bool:
    for program in self.servers:
      if program.process.start() == ProcessStatus.COULDNT_START:
        return False
    return True

  def start_in_new_thread(self):
    that = self
    class StartingThread(Thread):
      def __init__(self):
        Thread.__init__(self)

      def run(self):
        that.start_button.configure(state=tk.DISABLED)
        that.stop_button.configure(state=tk.DISABLED)
        that.start_all()
        that.start_button.configure(state=tk.NORMAL)
        that.stop_button.configure(state=tk.NORMAL)

    thread = StartingThread()
    thread.start()


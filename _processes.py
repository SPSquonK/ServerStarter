import os
import subprocess
import time
import shutil
import ctypes


class ProcessStatus:
  # Constants
  CHANGED_PROCESS = 0
  COULDNT_START = 1
  ALREADY_STARTED = 2
  STATUS_STARTED = 3
  STATUS_OFF = 4
  STATUS_OBSOLETE = 5

# Represents a generic program with a source file where is written the compiled program and a destination executable.
class Program:
  def __init__(self, executable, source, working_directory, args=[]):
    self.executable = executable
    self.source = source
    try :
      self.dateofexecutable = os.path.getmtime(self.executable)
    except:
      self.dateofexecutable = 0
    self.isuptodate = True
    self.functions = []
    self.cwd = working_directory
    self.args = args
  
  def kill_all_process(self):
    print("Abstract method : Not implemented")
  
  # Returns true if the stored executable is up to date according to the source
  def is_up_to_date(self):
    if not self.isuptodate:
      return False
    
    timestampofstored = None
    
    try:
      timestampofstored = os.path.getmtime(self.source)
    except FileNotFoundError:
      return True
    
    self.isuptodate = self.dateofexecutable >= timestampofstored
    
    return self.isuptodate
    
  # If needed, update the program. Kills the process if running. Returns true if the program has been updated
  def update(self):
    if self.is_up_to_date():
      return False
    
    self.kill_all_process()
    
    shutil.copy2(self.source, self.executable)
    self.dateofexecutable = os.path.getmtime(self.executable)
    self.isuptodate = True
    
    return True
    
  def start_a_new_process(self, startupinfo):
    command = [self.executable] + self.args
    return subprocess.Popen(command, -1, startupinfo=startupinfo, cwd=self.cwd)
    

class ProgramClient(Program):
  def __init__(self, executable, source, working_directory, args):
    Program.__init__(self, executable, source, working_directory, args)
    self.processes = []
      
  def kill_all_process(self):
    for process in self.processes:
      process.kill()
          
    while self.get_number_of_processes() != 0:
      time.sleep(0.1)
      
  def start_new_process(self):
    if self.get_number_of_processes() == 0:
      self.update()
  
    self.processes.append(self.start_a_new_process(None))
  
  def get_number_of_processes(self):
    self.processes = [p for p in self.processes if p.poll() is None]
  
    return len(self.processes)
        
  # Start a new process. If not up to date, kill every process to update the file
  def start_updated(self):
    self.kill_all_process()
    self.start_new_process()
        
  def open_ini(self):
    ini_file = self.executable[:-3] + "ini"
    os.startfile(ini_file)


# Represents a program which is part of a server (only one process concurrently, can have a dependencie)
class ProgramServer(Program):
  startuphidden = None

  @staticmethod
  def ensure_startup_exist():
    if ProgramServer.startuphidden is None:
      if os.name == 'nt':
        ProgramServer.startuphidden = subprocess.STARTUPINFO()
        ProgramServer.startuphidden.dwFlags |= subprocess.STARTF_USESHOWWINDOW

  # Initialize the program
  def __init__(self, name, executable, working_directory, possible_process_names, source, timeout, hide=True, arguments=[]):
    Program.__init__(self, executable, source, working_directory, arguments)

    # Passed properties
    self.name = name
    self.possible_names = possible_process_names
    self.timeout = timeout
    self.hide = hide
    self.dependencies = []
    self.relied_by = []
    
    ProgramServer.ensure_startup_exist()
    
    # Built properties
    self.process = None
      
  # Add a dependency
  def add_dependency(self, dependency):
    self.dependencies.append(dependency)
    dependency.relied_by.append(self)
      
  # Returns true if a process for this program is running
  def is_running(self):
    if self.process is None:
      return False
    
    if self.process.poll() is None:
      return True
    
    self.process = None
    return False
  
  # If the process is running, kills it
  def kill(self):
    if self.process is None:
      return
    
    self.process.kill()
    self.process = None

  def kill_all_process(self):
    for relied in self.relied_by:
      relied.kill_all_process()
    
    if self.is_running():
      self.kill()
  
  def get_status(self):
    if not self.is_running():
      return ProcessStatus.STATUS_OFF
    elif self.is_up_to_date():
      return ProcessStatus.STATUS_STARTED
    else:
      return ProcessStatus.STATUS_OBSOLETE
  
  def start(self):
    restart = False
    
    for dependencie in self.dependencies:
      status = dependencie.start()
      
      if status == ProcessStatus.COULDNT_START:
        return ProcessStatus.COULDNT_START
      elif status == ProcessStatus.CHANGED_PROCESS:
        restart = True
    
    if not self.is_up_to_date():
      self.update()
      restart = True
    
    if self.is_running() and not restart:
      return ProcessStatus.ALREADY_STARTED
    
    self.kill_all_process()
    return self.start_the_process()
  
  def solo_start(self):
    if not self.is_up_to_date():
      self.update()

    return self.start_the_process()

  def start_the_process(self):
    startupinfo = ProgramServer.startuphidden if self.hide else None
    
    self.process = self.start_a_new_process(startupinfo)
    
    time.sleep(self.timeout)

    if self.is_running():
      return ProcessStatus.CHANGED_PROCESS
    else:
      return ProcessStatus.COULDNT_START

  def change_visibility(self):
    user32 = ctypes.WinDLL("user32.dll")

    hwnd = None
    for possible_title in self.possible_names:
      hwnd = user32.FindWindowW(None, possible_title)
      if hwnd is not None:
        break

    if hwnd is None:
      return

    SW_HIDE = 0
    SW_SHOW = 5
    new_visibility = SW_HIDE if user32.IsWindowVisible(hwnd) else SW_SHOW
    user32.ShowWindow(hwnd, new_visibility)

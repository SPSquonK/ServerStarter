from _processes import ProgramClient, ProcessStatus
from _viewed_server import status_color, status_text

try:
  import Tkinter as tk
except ImportError:
  import tkinter as tk

class ViewedClientProcesses:
  process: ProgramClient
  controls: { 'state': tk.Message, 'quantity': tk.Message }

  @staticmethod
  def setup_from_json(json_content, gui_client_maker):
    state_message, quantity_message = gui_client_maker.build_client_frame()

    process = ProgramClient(
      json_content['run_path'],
      json_content['compile_path'],
      json_content['working_directory'],
      json_content['arguments'] if 'arguments' in json_content else []
    )
    
    list_of_client_controls = { 'state': state_message, 'quantity': quantity_message }

    result = ViewedClientProcesses(process, list_of_client_controls)
    result._add_buttons_and_listeners(gui_client_maker)
    return result

  def __init__(self, process: ProgramClient, controls):
    self.process = process
    self.controls = controls

  def _add_buttons_and_listeners(self, gui_client_maker):
    buttons = [
      { 'text': 'Kill all'     , 'command': self.process.kill_all_process },
      { 'text': 'Restart one'  , 'command': self.process.start_updated },
      { 'text': 'Start'        , 'command': self.process.start_new_process },
      { 'text': 'Open INI file', 'command': self.process.open_ini }
    ]

    gui_client_maker.add_client_buttons(buttons)

  def update_status(self):
    if self.process.is_up_to_date():
      text = "Neuz is up to date"
      color = status_color.get(ProcessStatus.STATUS_STARTED)
    else:
      text = "Neuz can be updated"
      color = status_color.get(ProcessStatus.STATUS_OBSOLETE)
    
    self.controls['state'].configure(text=text)
    self.controls['state'].configure(foreground=color)
    
    started_neuz = self.process.get_number_of_processes()
    
    if started_neuz == 0:
      text = "No neuz running"
      color = status_color.get(ProcessStatus.STATUS_OFF)
    else:
      text = str(started_neuz) + " neuz running"
      color = status_color.get(ProcessStatus.STATUS_STARTED)
    
    self.controls['quantity'].configure(text=text)
    self.controls['quantity'].configure(foreground=color)

